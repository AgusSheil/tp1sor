#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argcv[]){
    int num1,num2;
    int oper;

    printf("Introduzca el primer valor: ");
    scanf("%d",&num1);
    printf("Introduzca el segundo valor: ");
    scanf("%d",&num2);
    printf("Introduzca la operacion a realizar (+,-,*,/):");
    scanf("%d",&oper);

    if (oper==1){
        printf ("La suma entre %d y %d es: %d",num1,num2,num1+num2);
    }else if (oper==2){
        printf ("La resta entre %d y %d es: %d",num1,num2,num1-num2);
    }else if (oper==3){
        printf ("La multiplicación entre %d y %d es: %d",num1,num2,num1*num2);
    }else if (oper==4){
        printf ("La division entre %d y %d es: %d",num1,num2,num1/num2);
    }else{
        printf("No es un operador valido");
    }
    //system ("top");
    //system ("kill -TERM pid");
}
