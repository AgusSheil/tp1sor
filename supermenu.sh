#!/bin/bash
#------------------------------------------------------
# PALETA DE COLORES
#------------------------------------------------------
#setaf para color de letras/setab: color de fondo
	red=`tput setaf 1`;
	green=`tput setaf 2`;
	blue=`tput setaf 4`;
	bg_blue=`tput setab 4`;
	reset=`tput sgr0`;
	bold=`tput setaf bold`;

#------------------------------------------------------
# TODO: Completar con su path
#------------------------------------------------------
proyectoActual= $(pwd);


#------------------------------------------------------
# DISPLAY MENU
#------------------------------------------------------
imprimir_menu () {
       imprimir_encabezado "\t  S  U  P  E  R  -  M  E  N U ";
	
    echo -e "\t\t El proyecto actual es:";
    echo -e "\t\t $(pwd)";
    
    echo -e "\t\t";
    echo -e "\t\t Opciones:";
    echo "";
    echo -e "\t\t\t a.  Ver estado del proyecto";
    echo -e "\t\t\t b.  Guardar cambios";
    echo -e "\t\t\t c.  Actualizar repo";
    echo -e "\t\t\t f.  Abrir en terminal";        
    echo -e "\t\t\t g.  Abrir en carpeta"; 
    echo -e "\t\t\t h.  Crear carpetas";
    echo -e "\t\t\t i.  Eliminar carpetas";
    echo -e "\t\t\t j.  Configuracion estandar";
    echo -e "\t\t\t k.  Operaciones aritméticas";
    
    echo -e "\t\t\t q.  Salir";
    echo "";
    echo -e "Escriba la opción y presione ENTER";
}

#------------------------------------------------------
# FUNCTIONES AUXILIARES
#------------------------------------------------------

imprimir_encabezado () {
    clear;
    #Se le agrega formato a la fecha que muestra
    #Se agrega variable $USER para ver que usuario está ejecutando
    echo -e "`date +"%d-%m-%Y %T" `\t\t\t\t\t USERNAME:$USER";
    echo "";
    #Se agregan colores a encabezado
    echo -e "\t\t ${bg_blue} ${red} ${bold}--------------------------------------\t${reset}";
    echo -e "\t\t ${bold}${bg_blue}${red}$1\t\t${reset}";
    echo -e "\t\t ${bg_blue}${red} ${bold} --------------------------------------\t${reset}";
    echo "";
}

esperar () {
    echo "";
    echo -e "Presione enter para continuar";
    read ENTER ;
}

malaEleccion () {
    echo -e "Selección Inválida ..." ;
}

decidir () {
	echo $1;
	while true; do
		echo "desea ejecutar? (s/n)";
    		read respuesta;
    		case $respuesta in
        		[Nn]* ) break;;
       			[Ss]* ) eval $1
				break;;
        		* ) echo "Por favor tipear S/s ó N/n.";;
    		esac
	done
}

#------------------------------------------------------
# FUNCTIONES del MENU
#------------------------------------------------------
a_funcion () {
    	imprimir_encabezado "\tOpción a.  Ver estado del proyecto";
	echo "---------------------------"        
	echo "Something to commit?"
        decidir "cd $(pwd); git status";

        echo "---------------------------"        
	echo "Incoming changes (need a pull)?"
	decidir "cd $(pwd); git fetch origin"
	decidir "cd $(pwd); git log HEAD..origin/master --oneline"
}

b_funcion () {
       	imprimir_encabezado "\tOpción b.  Guardar cambios";
       	decidir "cd $(pwd); git add -A";
       	echo "Ingrese mensaje para el commit:";
       	read mensaje;
       	decidir "cd $(pwd); git commit -m \"$mensaje\"";
       	decidir "cd $(pwd); git push";
}

c_funcion () {
      	imprimir_encabezado "\tOpción c.  Actualizar repo";
      	decidir "cd $(pwd); git pull";   	 
}


f_funcion () {
	imprimir_encabezado "\tOpción f.  Abrir en terminal";        
	decidir "cd $(pwd); xterm &";
}

g_funcion () {
	imprimir_encabezado "\tOpción g.  Abrir en carpeta";        
	decidir "gnome-open $(pwd) &";
}

#------------------------------------------------------
# TODO: Completar con el resto de ejercicios del TP, una funcion por cada item
#------------------------------------------------------
h_funcion (){
    imprimir_encabezado "\tOpción h.  Crear carpetas";
    decidir "Crear carpetas";
    for numero in $(seq 0 99); do 
        if [ -d $numero ]; then
            echo "La carpeta $numero ya existe"
        else
            mkdir $numero;
            chmod -R 640 $numero;
        fi
    done
}
i_funcion (){
    imprimir_encabezado "\tOpción i.  Eliminar carpetas";
    decidir "Eliminar carpetas";
    for numero in $(seq 0 99); do 
        if [ -d $numero ]; then
            rmdir $numero;
        else
            echo "La carpeta $numero no existe"
        fi
    done
}
j_funcion (){
    imprimir_encabezado "\tOpción i.  Configuracion estandar";
    decidir "git config --global user.name 'emanuel';git config --global user.email 'dinardoema@gmail.com' ";
}

k_funcion (){
    imprimir_encabezado "\tOpción i.  Operaciones Aritmeticas";
    gcc estadoProceso.c -o ejecProc;
    cat estadoProceso.c;
    decidir "./ejecProc";  
}

#------------------------------------------------------
# LOGICA PRINCIPAL
#------------------------------------------------------
while  true
do
    # 1. mostrar el menu
    imprimir_menu;
    # 2. leer la opcion del usuario
    read opcion;
    
    case $opcion in
        a|A) a_funcion;;
        b|B) b_funcion;;
        c|C) c_funcion;;
        d|D) d_funcion;;
        e|E) e_funcion;;
        f|F) f_funcion;;
        g|G) g_funcion;;
        h|H) h_funcion;;
        i|I) i_funcion;;
        j|J) j_funcion;;
        k|K) k_funcion;;
        q|Q) break;;
        *) malaEleccion;;
    esac
    esperar;
done
